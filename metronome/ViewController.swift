//
//  ViewController.swift
//  metronome
//
//  Created by Wladyslaw Surala on 04/05/2019.
//  Copyright © 2019 Wladyslaw Surala. All rights reserved.
//

import Cocoa
import AudioToolbox

class ViewController: NSViewController {

    @IBOutlet weak var slider: NSSlider!
    
    var intValue = 150
    var timer: Timer? = nil
    var isplaying = false
    @IBOutlet var label : NSTextField?
    var counter=0
    var soundid : SystemSoundID = 1
    
    @IBAction func pushButton(sender: Any?) {
        if isplaying{
            self.stop()
            return
        }
        self.start()
    }
    
    func stop(){
        self.isplaying = false
        self.timer?.invalidate()
    }
    
    func start(){
        self.isplaying = true
        let interval = TimeInterval(60.0/Float(intValue))
        timer = Timer.scheduledTimer(withTimeInterval: interval, repeats: true, block: { (timer) in
            print("\(self.counter)")
            self.counter+=1
            AudioServicesPlaySystemSound(self.soundid)
        })
        timer?.fire()
    }
    

    @IBAction func sliderChanged(_ sender: NSSlider) {
        self.intValue = Int(sender.intValue)
        label?.stringValue = String(self.intValue)
        self.stop()
        self.start()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        label?.isEditable = false
        label?.stringValue = String(self.intValue)
        AudioServicesCreateSystemSoundID(Bundle.main.url(forResource: "2", withExtension: "aif")! as CFURL, &soundid)
        // Do any additional setup after loading the view.
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }


}

